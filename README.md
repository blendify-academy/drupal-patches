# Drupal patches

This document contains info about all patches which were applied for current site instance

## List of patches

**List of patches:**

* crossdomain_file.patch

_Patch adds crossdomain xml file for drupal core._

* 1289062.patch

[issue link](http://drupal.org/node/1289062)

_Patch fixes issue with empty file items validation for fields collections._

* 2101397.patch

[issue link](http://drupal.org/node/2101397)

_Patch fixes issue with NULL values for date items(days, months, years)._

* 2092789.patch

[issue link](https://drupal.org/node/2092789)

_Patch provides required option for multichoice title and question fields._

* PILOT-136_remove_stripes_on_ember_theme.patch

[issue link](https://jira-student.atlantis.ugent.be:8443/jira/browse/PILOT-136)

_Patch to remove the background stripes on the ember admin theme_

* 2173041-3-i-beam-menu-hover.patch

[issue link](https://jira-student.atlantis.ugent.be:8443/jira/browse/PILOT-241)

_Patch making hovering the navbar, display the point cursor again_

* PILOT-164_horizontal_tabs_on_ipad.patch

[issue link](https://jira-student.atlantis.ugent.be:8443/jira/browse/PILOT-164)

_Make sure the horizontal tabs get white after tapping the tab. (This is not done because iPad keeps the hover state)_

* PILOT-542_fix_oauth_avatar.patch

[issue link](https://jira-student.atlantis.ugent.be:8443/jira/browse/PILOT-542)

_Fixed the oauth import of linkedIn, so it also imports avatar/pictures and a link to the profile

* PILOT-658_no_js_error_when_iframe_other_domain.patch

[issue link](https://jira-student.atlantis.ugent.be:8443/jira/browse/PILOT-658)

_When drupal site with navbar is loaded inside another iframe from another domain, an js error occurs. This patch fixes so that no error is thrown.

* PILOT-733_dont_alert_ajax_error.patch

[issue link](https://jira-student.atlantis.ugent.be:8443/jira/browse/PILOT-733)

_When an ajax request gets cancelled (because of opening other page), Drupal will thrown an error. This error is not helpfull for the users, so log it instead

* PILOT-729_show_error_dropbox_fail.patch

[issue link](https://jira-student.atlantis.ugent.be:8443/jira/browse/PILOT-729)

_When dropbox does an upload, or the uploads fail, Drupal will now set a message, so the user knows if the upload was successfull

* PILOT-771_fix_hierarchical_oauth_data.patch

[issue link](https://jira-student.atlantis.ugent.be:8443/jira/browse/PILOT-771)

_When synchronising fields of external data providers with oauth, the fields are sometimes structured hierarchical. So with multidimensional arrays. But because the response is XML, it is displayed with Objects. When accessing data from the document, convert subarrays also to objects.

* PILOT-822_edit_form_on_top.patch

[issue link](https://jira-student.atlantis.ugent.be:8443/jira/browse/PILOT-822)

_When using the inline editor, the edit forms where hidding by elements that appeared above the floating panel. This is because complex z-index hierarchy with adding the form inline. No the form is added at the bottom and so is shown on top of everything.

* PILOT-860_IE10_scrolling.patch

[issue link](https://jira-student.atlantis.ugent.be:8443/jira/browse/PILOT-860)

_When scrolling in IE10, the page doesn't scroll with it. It stays on top, while the scrollbar does move. It is because the padding on the body is adapted with each scrolling. And that is a result for adding the navbar on top of the page.

* PILOT-887_cloudflare_flexible_ssl.patch

[issue link](https://jira-student.atlantis.ugent.be:8443/jira/browse/PILOT-887)

_Let the drupal core know when we are visiting pages over https (with cloudeflare flexible ssl), so it generates https urls instead of http urls.

* PILOT-887_cloudflare_flexible_ssl_oauth.patch

[issue link](https://jira-student.atlantis.ugent.be:8443/jira/browse/PILOT-887)

_Let the OAuth module know we are visiting pages of https (with cloudflare flexible SSL)

* PILOT-999_remove_use_of_devel_module.patch

[issue link](https://jira-student.atlantis.ugent.be:8443/jira/browse/PILOT-999)

_LTI tool provider - Remove use of devel module functions in lti_tool_provider_memberships._

* PILOT_1001_LTI_IMS_compliance.patch

[issue link](https://jira-student.atlantis.ugent.be:8443/jira/browse/PILOT-1001)

_LTI tool provider - IMS Global compliance_

* PILOT-1010_LTI_build_HTTP_query.patch

[issue link](https://jira-student.atlantis.ugent.be:8443/jira/browse/PILOT-1010)

_LTI tool provider - build HTTP query (increase consumer compatibility)._

* PILOT-1024_omega_undefined.patch

[issue link](https://jira-student.atlantis.ugent.be:8443/jira/browse/PILOT-1024)

_Omega theme - Remove the undefined property because of the misnaming of 'dir' instead of 'direction'

* PILOT-953_dont_redeclare_classes.patch

[issue link](https://jira-student.atlantis.ugent.be:8443/jira/browse/PILOT-953)

_Because of APC cache, the mysql file is loaded twice. Don't redeclare the classes.

* PILOT-953_inline_editor.patch

[issue link](https://jira-student.atlantis.ugent.be:8443/jira/browse/PILOT-953)

_Add some extra fixes for the inline editor

* drupal_core-empty-title-on-validation-2145483-12.patch

[issue link](https://jira-student.atlantis.ugent.be:8443/jira/browse/PILOT-1243)

